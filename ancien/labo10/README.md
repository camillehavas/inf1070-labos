# Laboratoire 10 - Scripts shell

## 1. Redirections et substitutions (30 minutes)

* Écrivez un script shell POSIX `ave` qui affiche « `Bonjour!` » sur la sortie
  standard et « `Au revoir!` » sur la sortie d'erreur standard
* Soit la commande shell « `./ave >b 2>a` »; que contiendrait les fichiers `a`
  et `b` ? Exécutez-la pour vérifier votre prédiction.
* Qu'afficherait la commande shell « `{ ./ave | rev ; } 2>&1 | rev` » ?
  Exécutez-la pour vérifier votre prédiction.
* Modifiez le script `ave` pour qu'il affiche le nom de l'utilisateur en plus
  en utilisant la variable d'environnement `USER`. Par exemple « `Bonjour
  privat!` » et « `Au revoir privat!` »
* Qu'afficherait maintenant la commande « `USER=Anonyme ./ave` » ?
  Exécutez-la pour vérifier votre prédiction.
* Modifiez le script `ave` pour utiliser la substitution de la commande
  « `id` » à la place de `USER`. Consultez les option de `id` pour avoir juste
  le nom de l'utilisateur.
* Qu'afficherait maintenant la commande « `USER=Anonyme ./ave` » ?
  Exécutez-la pour vérifier votre prédiction.

## 2. Évaluation des enseignements (interlude)

Faites l'évaluation en ligne des enseignements: http://www.evaluation.uqam.ca/

## 3. Scripts shell (60 minutes)

Dans les exercices qui suivent, référez-vous à la section *Structures de
contrôles* du chapitre 6 du cours: [Scripts
shell](http://info.uqam.ca/~privat/INF1070/06b-script.pdf)

### Structures conditionnelles et tests

Faites un script shell POSIX `foobar` qui prend un seul argument et l'affiche à
l'écran. Si l'argument est le nombre 3, le script affiche `foo` à la place.  Si
l'argument est le nombre 4, le script affiche `bar` à la place.  Si l'argument
est le nombre 12, le script affiche `foobar` à la place.

Exemple:

```sh
$ ./foobar toto
toto
$ ./foobar 30
30
$ ./foobar 3
foo
$ ./foobar 4
bar
$ ./foobar 12
foobar
$ ./foobar foo
foo
```

* Utilisez `if` et `[` (ou `test`)

### Filtrage

Modifiez `foobar` de telle sorte que si l'argument n'est pas un nombre entier
positif, le programme affiche un message d'erreur sur la sortie standard
d'erreur et se termine avec un code de retour de 1.

Exemple:

```sh
$ ./foobar 12
foobar
$ echo $?
0
$ ./foobar toto
foobar: toto n'est pas un entier
$ echo $?
1
```

* Vous pouvez tester si l'argument est un nombre entier avec `grep` et une
  expression régulière.

### Développement arithmétique

Modifiez `foobar` pour que le programme affiche `foo` pour tous les multiples
de 3 (et plus seulement pour le nombre 3 lui-même), `bar` pour les multiples de
4 et `foobar` pour les multiples de 12.

Exemple:

```sh
$ ./foobar 50
50
$ ./foobar 3
foo
$ ./foobar 30
foo
$ ./foobar 8
bar
$ ./foobar 240
foobar
```

* Utilisez le développement arithmétique `$(())`
* Note: l'opérateur `%` (modulo) retourne le reste de la division entière.
  « `x%y` » vaut `0` si `x` est un multiple de `y`.

### Boucle `for`

Modifiez `foobar` pour qu'il traite chaque nombre entre 1 et l'argument.

Exemple:

```
$ ./foobar 13
1
2
foo
bar
5
foo
7
bar
foo
10
11
foobar
13
$ foobar toto
foobar: toto n'est pas un entier
```

* Utilisez une boucle `for`
* Utilisez la commande `seq` pour générer une séquence de nombres entiers

### Boucle `while` (avancé)

Remplacez la boucle `for` (et le `seq`) par une boucle `while` et utilisez le
développement arithmétique pour incrémenter une variable de boucle.
