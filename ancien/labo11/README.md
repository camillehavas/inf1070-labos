# Laboratoire 11 - Réseau
## 1. Adresses IP (10 minutes)

- En ligne de commande, récupérez votre adresse IP privée à l'aide de la
  commande `ip`. Prenez-la en note.
- Ensuite, à l'aide de la commande `dig`, récupérez votre adresse IP publique.

## 2. *Socket* (30 minutes)

### 2.1. Bases de netcat

- Dans un terminal, entrez la commande `nc -l 3333 | rev`.
- Ouvrez un fureteur et rendez-vous à `localhost:3333`.
- Que voyez-vous apparaître dans le terminal?
- Rafraîchissez la page du fureteur. Est-ce que le terminal affiche de
  nouvelles informations? Pourquoi?
- Refaites le même exercice en ajoutant l'option `-k` à la commande `nc`.
  Au besoin, consultez le manuel (`man nc`).

### 2.2. Communications réseau

En équipe de deux étudiants, transmettez vous des messages par l'intermédiaire
de la commande netcat:

- Récupérez d'abord vos adresses IP respectives.
- Puis un premier étudiant ouvre une socket en mode « écoute » sur un port
  spécifique (par exemple 3333).
- Le deuxième étudiant envoie un message à l'aide de
  `echo message | nc adresse_ip`.
- Inversez ensuite les rôles.

## 3. Téléchargement (30 minutes)

### 3.1. Interrompre et relancer un téléchargement

- À l'aide de la commande `curl`, lancez le téléchargement de la ressource
  https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.9.tar.xz
- Interrompez-le avec `Ctrl-C`
- Puis relancez le téléchargement à l'aide de l'option `-C`.

### 3.2. Charger les locaux d'un cours

Sur le site de l'UQAM, il est possible d'afficher les locaux d'un cours en se
rendant sur la page https://etudier.uqam.ca. Par exemple, pour visualiser
l'horaire du cours INF1070, il suffit d'aller à l'adresse
https://etudier.uqam.ca/cours?sigle=inf1070.

Écrivez un petit script nommé `cours` qui prend en paramètre le sigle d'un
cours de l'UQAM (par exemple `inf1070`) et qui affiche sur la sortie standard
les locaux dans lesquels le cours se déroule.

Par exemple, on s'attend à ce que la commande

```sh
./cours inf1070
```

affiche le résultat

```sh
SH-R810
SH-3420
SH-3420
```

## 4. SSH et Tmux (40 minutes)

### 4.1. Connexion aux serveurs de Labunix

- À l'aide de la commande `ssh`, connectez-vous à la machine
  `java.labunix.uqam.ca`.
- Quels sont les autres utilisateurs connecté, et de quelle machines
  viennent-ils ? Utilisez la commande `who`.
- Explorez sommairement le système de fichiers disponible dans votre répertoire
  personnel.
- Quel est le résultat de `ls /home/`? Croyez-vous qu'il soit sécuritaire
  d'avoir accès à cette information?
- À l'aide de la commande `cd`, essayez de visiter le répertoire d'un autre
  utilisateur. Que se passe-t-il?
- Consultez les permissions des répertoires contenus dans `/home`.
- Déconnectez-vous du serveur `java`.
- Connectez-vous maintenant à la machine `zeta.labunix.uqam.ca`. Est-ce que les
  mêmes fichiers sont disponibles dans votre répertoire personnel? Pourquoi?

### 4.2. Génération de clés

- Générez une paire de clés RSA privée/publique en lançant la commande
  interactive `ssh-keygen` sans argument et en suivant les instructions.
- Affichez le contenu des clés publique et privée sur la sortie standard à
  l'aide de la commande `cat`.
- À l'aide de la commande `ssh-copy-id`, transférez votre clé publique
  (`id_rsa.pub`) sur le serveur `java`.
- Confirmez qu'il est maintenant possible de vous connecter sur `java` sans
  devoir saisir votre mot de passe.

### 4.3. Tmux

En consultant les dernières diapositives du chapitre 7 (réseau),
familiarisez-vous avec le logiciel Tmux:

- Connectez-vous en SSH sur le serveur `java`.
- Puis lancez une nouvelle session avec `tmux`.
- Créez plusieurs panneaux avec `Ctrl-B %` et `Ctrl-B "`.
- Changez la disposition des panneaux avec `Ctrl-B o` et `Espace`.
- Affichez l'heure avec `Ctrl-B t`.
- Détachez-vous de la session avec `Ctrl-B d`.
- Créez une deuxième session en relançant `tmux` puis quittez-la avec
  `Ctrl-B d`.
- Entrez la commande `tmux ls` pour lister les sessions existantes. Repérez les
  deux sessions que vous venez de créer.
- Chargez la première session que vous aviez créée avec `tmux a -t <nom>` où
  `<nom>` est remplacé par le nom de la session. Renommez-la session courante
  avec un nom plus facile à retenir et vérifiez le changement avec `Ctrl-B s`.
- Lancez le script [`ecrit`](./ecrit) disponible dans ce dépôt, détachez-vous
  de la session et déconnectez-vous du serveur `java`.
- Reconnectez-vous sur Java et chargez à nouveau la session `tmux` dans
  laquelle vous aviez lancé le script. Vérifiez que le script a bien continué
  de tourner malgré votre déconnection.
